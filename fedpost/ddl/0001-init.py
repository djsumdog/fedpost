from yoyo import step

steps = [
    step("""CREATE TABLE posts(
            id INTEGER PRIMARY KEY,
            name TEXT UNIQUE NOT NULL
    )""", "DROP TABLE posts"),
    step("""CREATE TABLE images(
            id INTEGER PRIMARY KEY,
            filename TEXT UNIQUE NOT NULL
    )""", "DROP TABLE images"),
    step("""CREATE TABLE post_images(
            id INTEGER PRIMARY KEY,
            post_id INTEGER,
            image_id INTEGER,
            FOREIGN KEY (post_id) REFERENCES posts(id),
            FOREIGN KEY (image_id) REFERENCES images(id),
            UNIQUE(post_id, image_id)
        )""", "DROP TABLE post_images"),
    step("""CREATE TABLE post_count(
            post_id INTEGER PRIMARY KEY,
            times_posted INTEGER,
            FOREIGN KEY (post_id) REFERENCES posts(id)
    )""", "DROP TABLE post_count"),
]
