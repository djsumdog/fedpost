from mastodon import Mastodon
import click

from os.path import isfile, join
from os import listdir, environ
import yaml
import logging
import random

from fedpost.db import Database

console = logging.StreamHandler()
console.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s'))
log = logging.getLogger('fedpost')
log.setLevel(logging.INFO)
log.addHandler(console)


def load_jekyll_posts(post_dir: str):
    posts = [f for f in listdir(post_dir) if isfile(join(post_dir, f))]
    meta_data = {}
    for p in posts:
        log.debug(f'Reading {p}')
        with open(join(post_dir, p), 'r') as fd:
            front_matter = next(yaml.load_all(fd, Loader=yaml.FullLoader))
            if 'title' in front_matter and 'photos' in front_matter:
                meta_data[front_matter['title']] = front_matter['photos']
            else:
                log.warn(f'No photo metadata for {p}')
    return meta_data


def load_photo_dir(photo_dir: str):
    posts = [f for f in listdir(photo_dir) if isfile(join(photo_dir, f))]
    post_map = {}
    for p in posts:
        parts = p.rsplit('-')
        if parts[0] not in post_map:
            post_map[parts[0]] = []
        if 'original' in p:
            post_map[parts[0]].append(p)
    return post_map


@click.command()
@click.option('--mastodon-url', envvar='MASTODON_URL', required=True)
@click.option('--mastodon-token', envvar='MASTODON_TOKEN', required=True)
@click.option('--db-file', envvar='DB_FILE', required=True)
#@click.option('--jekyll-posts', envvar='JEKYLL_POSTS', required=True)
@click.option('--photo-dir', envvar='PHOTO_DIR', required=True)
def main(mastodon_url, mastodon_token, db_file, photo_dir):

    log.info(f'Posting to {mastodon_url}')
    client = Mastodon(
      access_token = mastodon_token,
      api_base_url = mastodon_url
    )

    db = Database(db_file)
    db.migrate_schema()

    log.info('Loading posts...')
    # posts = load_jekyll_posts(jekyll_posts)
    posts = load_photo_dir(photo_dir)
    for post,images in posts.items():
        db.add_post(post, images)
    log.info('Posts Loaded')

    candidates = db.post_candidates()
    log.info(f'Potential Candidate Posts: {len(candidates)}')
    select_id = random.choice(candidates)
    log.info(f'Publishing {select_id}')
    image_files = db.images_for_post(select_id)

    media_uploads = []
    for f in image_files[:4]:
        # name,ext = f.rsplit('.', 1)
        # img_file = f'{name}-original.{ext}'
        img = join(photo_dir, f)
        log.info(f'Adding media: {img}')
        media_uploads.append(client.media_post(img))
    log.info('Posting...')
    client.status_post('', media_ids=media_uploads)

    db.increment_post_count(select_id)


if __name__ == '__main__':
    main()
