from yoyo import read_migrations
from yoyo import get_backend
import sqlite3
from os import path
from threading import Lock


class Database(object):

    def __init__(self, db_file):
        self.db_file = db_file
        self.migrate_schema()
        self.conn = sqlite3.connect(self.db_file, check_same_thread=False)
        self.sql_lock = Lock()

    def run_query(self, sql, args=[]):
        self.sql_lock.acquire()
        cursor = self.conn.cursor()
        result = cursor.execute(sql, args)
        self.conn.commit()
        self.sql_lock.release()
        return result

    def run_insert(self, sql, args=[]):
        self.sql_lock.acquire()
        cursor = self.conn.cursor()
        cursor.execute(sql, args)
        new_id = cursor.lastrowid
        self.conn.commit()
        self.sql_lock.release()
        return new_id

    def migrate_schema(self):
        backend = get_backend('sqlite:////{}'.format(self.db_file))
        ddl_dir = path.join(path.dirname(path.realpath(__file__)), 'ddl')
        migrations = read_migrations(ddl_dir)
        with backend.lock():
            backend.apply_migrations(backend.to_apply(migrations))

    def add_post(self, post_name: str, images: []):
      sql = 'INSERT OR IGNORE INTO posts(name) VALUES(?)'
      self.run_insert(sql, [post_name])
      post_id = None
      image_id = None
      for r in self.run_query('SELECT p.id FROM posts p WHERE p.name=?', [post_name]):
          post_id = r[0]
      self.run_query('INSERT OR IGNORE INTO post_count(post_id, times_posted) VALUES(?, 0)', [post_id])
      for i in images:
          self.run_insert('INSERT OR IGNORE INTO images(filename) VALUES(?)', [i])
          for r in self.run_query('SELECT i.id FROM images i WHERE i.filename=?', [i]):
              image_id = r[0]
          self.run_insert('INSERT OR IGNORE INTO post_images(post_id, image_id) VALUES(?,?)', [post_id, image_id])

    def images_for_post(self, post_id):
        mapper = []
        for r in self.run_query('SELECT i.filename FROM post_images pi JOIN posts p ON p.id=pi.post_id JOIN images i ON i.id=pi.image_id WHERE p.id=?', [post_id]):
            mapper.append(r[0])
        return mapper

    def post_candidates(self):
        sql = 'SELECT pc.post_id FROM post_count pc WHERE times_posted < (SELECT MAX(times_posted) FROM post_count)'
        posts = []
        for row in self.run_query(sql):
            posts.append(row[0])
        if len(posts) == 0:
            for row in self.run_query('SELECT pc.post_id FROM post_count pc'):
                posts.append(row[0])
        return posts

    def increment_post_count(self, post_id):
        sql = 'UPDATE post_count SET times_posted=times_posted + 1 WHERE post_id=?'
        self.run_query(sql, [post_id])
