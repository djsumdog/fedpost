FROM python:3.8

RUN pip install poetry

RUN mkdir /project
COPY pyproject.toml /project
COPY poetry.lock /project
WORKDIR /project
RUN poetry install

COPY fedpost /project/fedpost

ENTRYPOINT poetry run python -m fedpost.__main__
